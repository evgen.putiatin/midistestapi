﻿using Microsoft.AspNetCore.Identity;

namespace MidisTest
{
    public class Role : IdentityRole, IEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}
