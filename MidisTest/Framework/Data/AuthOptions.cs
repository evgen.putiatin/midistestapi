﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace MidisTest
{
    public class AuthOptions
    {
        public const string ISSUER = "AuthServer";
        public const string AUDIENCE = "AuthClient";
        private const string KEY = "5i0w9PwcrBpzGjfEGfWw";
        public const int LifeDay = 365;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
