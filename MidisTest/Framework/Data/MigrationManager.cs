﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace MidisTest
{
    public static class MigrationManager
    {
        private static string[] roles = new string[] { "Administrator", "Consultant", "Teacher", "Instructor" };
        public static IHost MigrateDatabase(this IHost host)
        {
            var scope = host.Services.CreateScope();
            UserManager<User> _userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
            RoleManager<Role> _roles = scope.ServiceProvider.GetRequiredService<RoleManager<Role>>();
            var appContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            try
            {
                appContext.Database.Migrate();
                SeedDatabase(appContext, _userManager, _roles);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[MigrateDatabase Exception]: {ex.Message}, inner exception: {ex.InnerException}");
                throw;
            }
            return host;
        }

        public static async void SeedDatabase(ApplicationDbContext context, UserManager<User> _userManager,
                RoleManager<Role> _roleManager)
        {
            context.Database.EnsureCreated();

            foreach (var role in roles)
            {
                if (!context.Roles.Any(r => r.Name == role))
                {
                    await _roleManager.CreateAsync(new Role() { Name = role });
                }
            }

            User user = context.Users.ToList().FirstOrDefault();
            if (user == default)
            {
                User _user = new User()
                {
                    Email = "admin@gmail.com",
                    UserName = "admin@gmail.com",
                    FirstName = "Admin",
                    LastName = "Admin",
                    EmailConfirmed = true
                };
                IdentityResult result = await _userManager.CreateAsync(_user, "Admin12345");
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(_user, RoleEnum.Administrator.ToString());
                }
            };
        }
    }
}