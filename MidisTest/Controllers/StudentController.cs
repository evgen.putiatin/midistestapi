﻿using Microsoft.AspNetCore.Mvc;
using MidisTester;

namespace MidisTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IGetStudent _getStudent;
        private readonly ICreateStudent _createStudent;
        private readonly IUpdateStudent _updateStudent;
        private readonly IDeleteStudent _deleteStudent;

        public StudentController(
            IGetStudent getStudent, 
            ICreateStudent createStudent, 
            IUpdateStudent updateStudent, 
            IDeleteStudent deleteStudent)
        {
            _getStudent = getStudent;
            _createStudent = createStudent;
            _updateStudent = updateStudent;
            _deleteStudent = deleteStudent;
        }

        [HttpGet]
        [Route("{studentId}")]
        public async Task<StudentDto> Get(string studentId)
        {
            return await _getStudent.Get(studentId);
        }

        [HttpGet]
        [Route("list")]
        public async Task<List<StudentDto>> List()
        {
            return await _getStudent.List();
        }

        [HttpPost]
        [Route("create")]
        public async Task<StudentDto> Create([FromBody] SaveStudentDto request)
        {
            return await _createStudent.Create(request);
        }

        [HttpPost]
        [Route("update")]
        public async Task<StudentDto> Update([FromBody] SaveStudentDto request)
        {
            return await _updateStudent.Update(request);
        }

        [HttpDelete]
        [Route("studentId")]
        public async Task Delete(string studentId)
        {
            await _deleteStudent.Delete(studentId);
        }
    }
}
