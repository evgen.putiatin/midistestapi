﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MidisTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogin _login;

        public AccountController(ILogin login)
        {
            _login = login;
        }

        [HttpPost]
        [Route("login")]
        public async Task<AccountDto> Login([FromBody] LoginDto request)
        {
            return await _login.Execute(request);
        }
    }
}
