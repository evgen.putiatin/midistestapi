﻿using Microsoft.AspNetCore.Mvc;
using MidisTest.UseCase.Customer;

namespace MidisTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IGetCustomer _getCustomer;
        private readonly ICreateCustomer _createCustomer;
        private readonly IUpdateCustomer _updateCustomer;
        private readonly IDeleteCustomer _deleteCustomer;

        public CustomerController(
            IGetCustomer getCustomer, 
            ICreateCustomer createCustomer, 
            IUpdateCustomer updateCustomer, 
            IDeleteCustomer deleteCustomer)
        {
            _getCustomer = getCustomer;
            _createCustomer = createCustomer;
            _updateCustomer = updateCustomer;
            _deleteCustomer = deleteCustomer;
        }

        [HttpGet]
        [Route("{customerId}")]
        public async Task<CustomerDto> Get(string customerId)
        {
            return await _getCustomer.Get(customerId);
        }

        [HttpGet]
        [Route("list")]
        public async Task<List<CustomerDto>> List()
        {
            return await _getCustomer.List();
        }

        [HttpPost]
        [Route("create")]
        public async Task<CustomerDto> Create([FromBody] CreateCustomerDto request)
        {
            return await _createCustomer.Create(request);
        }

        [HttpPost]
        [Route("update")]
        public async Task<CustomerDto> Update([FromBody] SaveCustomerDto request)
        {
            return await _updateCustomer.Update(request);
        }

        [HttpDelete]
        [Route("customerId")]
        public async Task Delete(string customerId)
        {
            await _deleteCustomer.Delete(customerId);
        }
    }
}
