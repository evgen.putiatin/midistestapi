﻿using Microsoft.EntityFrameworkCore;

namespace MidisTest
{
    public interface IDeleteStudent
    {
        public Task Delete(string studentId);
    }

    public class DeleteStudent : IDeleteStudent
    {
        private readonly ApplicationDbContext _context;

        public DeleteStudent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Delete(string studentId)
        {
            var query = await _context.Students
                .FirstOrDefaultAsync(x => x.Id == studentId);

            if (query != null)
            {
                _context.Remove(query);
                await _context.SaveChangesAsync();
            }
        }
    }
}
