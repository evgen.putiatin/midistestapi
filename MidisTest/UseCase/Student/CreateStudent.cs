﻿using AutoMapper;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;

namespace MidisTest
{
    public interface ICreateStudent
    {
        public Task<StudentDto> Create(SaveStudentDto request);
    }

    public class CreateStudent : ICreateStudent
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public CreateStudent(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<StudentDto> Create(SaveStudentDto request)
        {
            var saveStudent = _mapper.Map<Student>(request);
            saveStudent.Id = Guid.NewGuid().ToString();
            await _context.AddAsync(saveStudent);
            _context.SaveChanges();

            return _mapper.Map<StudentDto>(saveStudent);
        }
    }
}
