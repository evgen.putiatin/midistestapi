﻿using AutoMapper;

namespace MidisTest
{
    public class StudentMapper : Profile
    {
        public StudentMapper()
        {
            CreateMap<Student, StudentDto>();
            CreateMap<SaveStudentDto, Student>();
        }
    }
}
