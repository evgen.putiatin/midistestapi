﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MidisTest;
using System.Reflection.Metadata;

namespace MidisTester
{
    public interface IGetStudent
    {
        public Task<StudentDto> Get(string studentId);
        public Task<List<StudentDto>> List();
    }

    public class GetStudent : IGetStudent
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetStudent(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<StudentDto> Get(string studentId)
        {
            var query = await _context.Students
                .Where(x => x.Id == studentId)
                .FirstOrDefaultAsync();

            return _mapper.Map<StudentDto>(query);
        }

        public async Task<List<StudentDto>> List()
        {
            var query = await _context.Students
               .ToListAsync();

            return _mapper.Map<List<StudentDto>>(query);
        }
    }
}
