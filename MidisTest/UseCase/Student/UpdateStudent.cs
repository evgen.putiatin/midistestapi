﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace MidisTest
{
    public interface IUpdateStudent
    {
        public Task<StudentDto> Update(SaveStudentDto request);
    }

    public class UpdateStudent : IUpdateStudent
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UpdateStudent(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<StudentDto> Update(SaveStudentDto request)
        {
            var query = await _context.Students
               .FirstOrDefaultAsync(x => x.Id == request.Id);

            if (query != default)
            {
                query = _mapper.Map<Student>(request);
                _context.Update(query);
                _context.SaveChanges();
            }
            return _mapper.Map<StudentDto>(query);
        }
    }
}
