﻿namespace MidisTest
{
    public class SaveStudentDto
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirstYear { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public TrainingCategory TrainingCategory { get; set; }
        public bool Theory { get; set; }
        public bool PracticalDriving { get; set; }
        public DateTime? ExamDate { get; set; }
    }
}
