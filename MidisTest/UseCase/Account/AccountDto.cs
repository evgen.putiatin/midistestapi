﻿namespace MidisTest
{
    public class AccountDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Token { get; set; }
        public bool Successful { get; set; }
        public string ErrorMessage { get; set; }
    }
}
