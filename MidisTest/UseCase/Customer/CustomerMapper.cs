﻿using AutoMapper;

namespace MidisTest
{
    public class CustomerMapper : Profile
    {
        public CustomerMapper()
        {
            CreateMap<SaveCustomerDto, User>();
            CreateMap<User, CustomerDto>();
        }
    }
}
