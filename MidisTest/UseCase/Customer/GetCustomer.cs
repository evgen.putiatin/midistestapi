﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace MidisTest
{
    public interface IGetCustomer
    {
        public Task<CustomerDto> Get(string customerId);
        public Task<List<CustomerDto>> List();
    }

    public class GetCustomer : IGetCustomer
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCustomer(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CustomerDto> Get(string customerId)
        {
            var query = await _context.Users
                .Where(x => x.Id == customerId)
                .FirstOrDefaultAsync();

            return _mapper.Map<CustomerDto>(query);
        }

        public async Task<List<CustomerDto>> List()
        {
            var query = await _context.Users
               .ToListAsync();

            return _mapper.Map<List<CustomerDto>>(query);
        }
    }
}
