﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MidisTest.UseCase.Customer;

namespace MidisTest
{
    public interface ICreateCustomer
    {
        public Task<CustomerDto> Create(CreateCustomerDto request);
    }

    public class CreateCustomer : ICreateCustomer
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roles;
        private readonly IMapper _mapper;

        public CreateCustomer(
            ApplicationDbContext context, 
            IMapper mapper, 
            UserManager<User> userManager, 
            RoleManager<Role> roles)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _roles = roles;
        }

        public async Task<CustomerDto> Create(CreateCustomerDto request)
        {
            User user = await _context.Users.FirstOrDefaultAsync(x => x.Email == request.Email);
            if (user == default) {
                user = new User();
                user.Email = request.Email;
                user.UserName = request.Email;
                user.FirstName = request.FirstName;
                user.LastName = request.LastName;

                IdentityResult result = await _userManager.CreateAsync(user, "Admin12345");
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, RoleEnum.Administrator.ToString());
                }
            }
            return _mapper.Map<CustomerDto>(user);
        }
    }
}
