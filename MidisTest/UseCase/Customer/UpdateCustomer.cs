﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace MidisTest
{
    public interface IUpdateCustomer
    {
        public Task<CustomerDto> Update(SaveCustomerDto request);
    }

    public class UpdateCustomer : IUpdateCustomer
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UpdateCustomer(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CustomerDto> Update(SaveCustomerDto request)
        {
            var query = await _context.Users
               .FirstOrDefaultAsync(x => x.Id == request.Id);

            if (query != default)
            {
                query = _mapper.Map<User>(request);
                _context.Update(query);
                _context.SaveChanges();
            }
            return _mapper.Map<CustomerDto>(query);
        }
    }
}
