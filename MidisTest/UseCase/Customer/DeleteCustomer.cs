﻿using Microsoft.EntityFrameworkCore;

namespace MidisTest
{
    public interface IDeleteCustomer
    {
        public Task Delete(string customerId);
    }

    public class DeleteCustomer : IDeleteCustomer
    {
        private readonly ApplicationDbContext _context;

        public DeleteCustomer(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Delete(string customerId)
        {
            var query = await _context.Users
                .FirstOrDefaultAsync(x => x.Id == customerId);

            if (query != null)
            {
                _context.Remove(query);
                await _context.SaveChangesAsync();
            }
        }
    }
}
