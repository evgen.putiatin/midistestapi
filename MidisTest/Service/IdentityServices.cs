﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace MidisTest
{
    public class IdentityServices
    {
        private readonly UserManager<User> _userManager;
        private ApplicationDbContext _dbContext; 
        public IdentityServices(UserManager<User> context, ApplicationDbContext dbContext)
        {
            _userManager = context;
            _dbContext = dbContext;
        }
        public async Task<AccountDto> GetIdentity(User user, string password, AccountDto response)
        {
            ClaimsIdentity claimsIdentity = default;
            if (user != null && await _userManager.CheckPasswordAsync(user, password))
            {
                string roleId = _dbContext.UserRoles.FirstOrDefault(x => x.UserId == user.Id).RoleId;
                string role = _dbContext.Roles.FirstOrDefault(x => x.Id == roleId).Name;

                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                    new Claim("role", role),
                    new Claim("firstName", user.FirstName ),
                    new Claim("lastName", user.LastName ),
                };

                claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

                response.Id = user.Id;
                response.UserName = user.UserName;
                response.Email = user.Email;
                response.Token = GetJwtSecurityToken(claimsIdentity);
                response.ExpireDate = DateTime.Now.AddDays(AuthOptions.LifeDay);
                response.Successful = true;
                return response;
            }
            else
            {
                response.ErrorMessage = "Invalid username or password.";
                return response;
            }
        }
        private string GetJwtSecurityToken(ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.AddDays(AuthOptions.LifeDay),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
    }
}
